-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 24 Maj 2021, 19:26
-- Wersja serwera: 10.4.11-MariaDB
-- Wersja PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `faktury`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `faktury`
--

CREATE TABLE `faktury` (
  `id_faktury` int(11) NOT NULL,
  `fk_id_kontrahenta` int(11) DEFAULT NULL,
  `data_wystawienia` date NOT NULL DEFAULT current_timestamp(),
  `termin_platnosci` date NOT NULL,
  `sposob_platnosci` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL DEFAULT 'przelew',
  `do_zaplaty` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `faktury`
--

INSERT INTO `faktury` (`id_faktury`, `fk_id_kontrahenta`, `data_wystawienia`, `termin_platnosci`, `sposob_platnosci`, `do_zaplaty`) VALUES
(1, 1, '2021-05-24', '2021-05-31', 'przelew', '122'),
(2, 21, '2021-05-24', '2021-05-31', 'Gotówka', '23,96'),
(3, 1, '2021-05-24', '2021-05-31', 'Gotówka', '41,94'),
(4, 2, '2021-05-24', '2021-05-31', 'Gotówka', '113,9'),
(5, 20, '2021-05-24', '2021-05-31', 'Gotówka', '50,95'),
(6, 21, '2021-05-24', '2021-05-31', 'Przelew', '149,9');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `firma`
--

CREATE TABLE `firma` (
  `id_firmy` int(11) NOT NULL,
  `nazwa_firmy` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL,
  `adres_firmy` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL,
  `nip_firmy` varchar(20) COLLATE utf8mb4_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `firma`
--

INSERT INTO `firma` (`id_firmy`, `nazwa_firmy`, `adres_firmy`, `nip_firmy`) VALUES
(0, 'NIE DODANO NAZWY FIRMY', 'NIE DODANO ADRESU FIRMY', '000 00 00 000'),
(1, 'TomKos', 'Grota Roweckiego 9F/16', '725-18-01-126'),
(2, 'Relef', 'Hubala 123/4', '23234234 4234242'),
(12, 'Koseczkowo', 'Dambonia 157/53', '725-18-01-127');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kontrahenci`
--

CREATE TABLE `kontrahenci` (
  `id_kontrahenta` int(11) NOT NULL,
  `nazwisko_kontrahenta` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL,
  `imie_kontrahenta` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL,
  `telefon` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_polish_ci NOT NULL,
  `fk_id_firmy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `kontrahenci`
--

INSERT INTO `kontrahenci` (`id_kontrahenta`, `nazwisko_kontrahenta`, `imie_kontrahenta`, `telefon`, `email`, `fk_id_firmy`) VALUES
(1, 'Kosek', 'Tomasz', '694815770', 'kosektomasz96@gmail.com', 1),
(2, 'Kowalski', 'Rafał', '694815770', 'aeaeaseaasd@gmail.com', 2),
(19, 'Majewski', 'Grzegorz', '777999888', 'maj@grze@wp.pl', 0),
(20, 'Adamowicz', 'Adam', '555666777', 'a.adamowicz@wp.pl', 2),
(21, 'Cyganiuk', 'Grzegorz', '444555666', 'cyganiuk.g@wp.pl', 12),
(22, 'Tureczek', 'Marian', '666666666', 'turek.marian@wp.pl', 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `produkty`
--

CREATE TABLE `produkty` (
  `id_produktu` int(11) NOT NULL,
  `nazwa_produktu` varchar(255) COLLATE utf8mb4_polish_ci DEFAULT NULL,
  `cena_produktu` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `produkty`
--

INSERT INTO `produkty` (`id_produktu`, `nazwa_produktu`, `cena_produktu`) VALUES
(3, 'SilentiumPC Pactum PT-1 4G', 14.99),
(4, 'Verbatim CD-R Extra Protection Slim Case', 2.99);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zamowienia`
--

CREATE TABLE `zamowienia` (
  `fk_id_faktury` int(11) DEFAULT NULL,
  `fk_id_produktu` int(11) DEFAULT NULL,
  `ilosc` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `zamowienia`
--

INSERT INTO `zamowienia` (`fk_id_faktury`, `fk_id_produktu`, `ilosc`) VALUES
(2, 4, 3),
(2, 3, 1),
(3, 3, 2),
(3, 4, 4),
(4, 4, 3),
(4, 3, 7),
(5, 3, 3),
(5, 4, 2),
(6, 3, 10);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `faktury`
--
ALTER TABLE `faktury`
  ADD PRIMARY KEY (`id_faktury`);

--
-- Indeksy dla tabeli `firma`
--
ALTER TABLE `firma`
  ADD PRIMARY KEY (`id_firmy`);

--
-- Indeksy dla tabeli `kontrahenci`
--
ALTER TABLE `kontrahenci`
  ADD PRIMARY KEY (`id_kontrahenta`);

--
-- Indeksy dla tabeli `produkty`
--
ALTER TABLE `produkty`
  ADD PRIMARY KEY (`id_produktu`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `faktury`
--
ALTER TABLE `faktury`
  MODIFY `id_faktury` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=622;

--
-- AUTO_INCREMENT dla tabeli `firma`
--
ALTER TABLE `firma`
  MODIFY `id_firmy` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT dla tabeli `kontrahenci`
--
ALTER TABLE `kontrahenci`
  MODIFY `id_kontrahenta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT dla tabeli `produkty`
--
ALTER TABLE `produkty`
  MODIFY `id_produktu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
