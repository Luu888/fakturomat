﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.draw;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fakturomat
{
    public partial class Dane_do_faktury : Form
    {
        string index_pobrany;
        string ostatni_id = "";
        public Dane_do_faktury(string v)
        {
            InitializeComponent();
            index_pobrany = v;
            dane_kontrahenta();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            generuj_fakture();
        }
        public void generuj_fakture()
        {

                FileStream fs = new FileStream((Convert.ToInt32(ostatni_id)+1).ToString()+".pdf", FileMode.Create, FileAccess.Write, FileShare.None);
                Document doc = new Document();
                PdfWriter writer = PdfWriter.GetInstance(doc, fs);
                doc.Open();
                iTextSharp.text.Font myFont = FontFactory.GetFont("Arial", 14, iTextSharp.text.Font.BOLD, new iTextSharp.text.BaseColor(0, 0, 0));
                doc.Add(new Paragraph("Sprzedajacy", myFont));
                Chunk linebreak = new Chunk(new LineSeparator());
                
                iTextSharp.text.Font myFont2 = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0));
                iTextSharp.text.Font myFont3 = FontFactory.GetFont("Arial", 16, iTextSharp.text.Font.NORMAL, new iTextSharp.text.BaseColor(0, 0, 0));
                doc.Add(new Paragraph("Uslugi informatyczne Kosek Tomasz", myFont2));
                doc.Add(new Paragraph("NIP: 123-456-78-19", myFont2));
                doc.Add(new Paragraph("Dambonia 157/66", myFont2));
                doc.Add(new Paragraph("45-861 Opole", myFont2));
            doc.Add(linebreak);
            doc.Add(new Paragraph("Kupujacy", myFont));

            doc.Add(new Paragraph(firmaBox.Text+ " " + nazwiskoBox.Text + " "+ ImieBox.Text, myFont2));
            doc.Add(new Paragraph(NIPBox.Text, myFont2));
            doc.Add(new Paragraph(AdresBox.Text, myFont2));
            doc.Add(linebreak);
            PdfPTable table = new PdfPTable(4);
            Phrase ph = new Phrase("Faktura VAT nr " + (Convert.ToInt32(ostatni_id) + 1).ToString()+"/"+DateTime.Now.ToString("yyyy"), myFont3);
            PdfPCell cell = new PdfPCell(ph);
            cell.Colspan = 4;
            cell.HorizontalAlignment = 1;
            cell.VerticalAlignment = 1;
            table.AddCell(cell);
            table.AddCell("ID produktu");
            table.AddCell("Nazwa produktu");
            table.AddCell("Cena produktu (w zl)");
            table.AddCell("Ilosc");
            //SELECT id_produktu, nazwa_produktu, cena_produktu, zamowienia.ilosc from produkty join zamowienia on zamowienia.fk_id_produktu = id_produktu
            string connStr = "server=localhost;user=root;database=faktury;port=3306;password=";
            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();
            int id = Convert.ToInt32(ostatni_id) + 1;
            string sql = "SELECT id_produktu, nazwa_produktu, cena_produktu, zamowienia.ilosc from produkty join zamowienia on zamowienia.fk_id_produktu = id_produktu where fk_id_faktury="+id;
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                table.AddCell(rdr[0].ToString());
                table.AddCell(rdr[1].ToString());
                table.AddCell(rdr[2].ToString());
                table.AddCell(rdr[3].ToString());
            }
            conn.Close();
            doc.Add(table);
            string sposob_platnosci = "";
            if (radioButton1.Checked == true)
            {
                sposob_platnosci = "Przelew";
            }
            else
            {
                sposob_platnosci = "Gotówka";
            }
            DateTime today = DateTime.Now;
            DateTime answer = today.AddDays(7);
            doc.Add(new Paragraph("Sposob platnosci: "+sposob_platnosci, myFont2));
            doc.Add(new Paragraph("Termin platnosci: "+answer.ToString("dd-MM-yyyy"), myFont2));
            doc.Add(linebreak);
            doc.Add(new Paragraph("Do zaplaty: " + label10.Text.ToString()+ " zl", myFont3));

            doc.Close();

        }
        public void dane_kontrahenta()
        {
            string connStr = "server=localhost;user=root;database=faktury;port=3306;password=";
            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();
            string sql = "SELECT * FROM kontrahenci JOIN firma on fk_id_firmy=id_firmy WHERE id_kontrahenta = " + index_pobrany;
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                ImieBox.Text = rdr[2].ToString();
                nazwiskoBox.Text = rdr[1].ToString();
                telefonBox.Text = rdr[3].ToString();
                emailBox.Text = rdr[4].ToString();
                firmaBox.Text = rdr[7].ToString();
                AdresBox.Text = rdr[8].ToString();
                NIPBox.Text = rdr[9].ToString();
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            new dodawanie_do_zamowienia().Show();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            zapis_faktury_do_bazy();
        }
        public void zapis_faktury_do_bazy()
        {
            string connStr = "server=localhost;user=root;database=faktury;port=3306;password=";
            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();
            string sql = "SELECT MAX(id_faktury) as id FROM faktury";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();
            
            while (rdr.Read())
            {
                ostatni_id = rdr[0].ToString();
            }
            conn.Close();
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                conn.Open();
                string dodawanie_do_zamowienia = "INSERT INTO zamowienia VALUES(" + (Convert.ToInt32(ostatni_id) + 1) + ", '"+row.Cells["IDproduktu"].Value +"', '"+ row.Cells["Ilosc"].Value + "')";
                MySqlCommand cmd1 = new MySqlCommand(dodawanie_do_zamowienia, conn);
                MySqlDataReader rdr1 = cmd1.ExecuteReader();
                conn.Close();
                generuj_fakture();
            }




            conn.Open();
            DateTime today = DateTime.Now;
            DateTime answer = today.AddDays(7);
            string sposob_platnosci = "";
            if (radioButton1.Checked == true)
            {
                sposob_platnosci = "Przelew";
            }
            else
            {
                sposob_platnosci = "Gotówka";
            }
            string sql2 = "INSERT INTO faktury Values(" + (Convert.ToInt32(ostatni_id) + 1) + ", '" + index_pobrany + "', '" + DateTime.Now.ToString("yyyy-MM-dd") + "', '" + answer.ToString("yyyy-MM-dd") + "', '" + sposob_platnosci + "', '" + label10.Text.ToString() + "')";
            MySqlCommand cmd2 = new MySqlCommand(sql2, conn);
            MySqlDataReader rdr2 = cmd2.ExecuteReader();
        }
    }
}
