﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fakturomat
{
    public partial class wykaz_faktur : Form
    {
        List<string> faktury_id = new List<string>();
        public wykaz_faktur()
        {
            InitializeComponent();
        }

        private void wykaz_faktur_Load(object sender, EventArgs e)
        {
            pokaz_faktury();
        }
        public void pokaz_faktury()
        {
            string connStr = "server=localhost;user=root;database=faktury;port=3306;password=";
            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();
            string sql = "SELECT * FROM faktury ORDER BY id_faktury DESC";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                comboBox1.Items.Add((rdr[0] + "/" + rdr[2]).ToString());
                faktury_id.Add(rdr[0].ToString());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            pokaz_konkretna_fakture();
        }
        public void pokaz_konkretna_fakture()
        {
            string index = faktury_id[comboBox1.SelectedIndex];
            string connStr = "server=localhost;user=root;database=faktury;port=3306;password=";
            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();
            string sql = "SELECT * FROM faktury JOIN kontrahenci ON faktury.fk_id_kontrahenta=kontrahenci.id_kontrahenta JOIN firma on kontrahenci.fk_id_firmy=firma.id_firmy WHERE id_faktury=" + index;
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                nazwiskoBox.Text = (rdr[7]).ToString();
                ImieBox.Text = (rdr[8]).ToString();
                telefonBox.Text = (rdr[9]).ToString();
                emailBox.Text = (rdr[10]).ToString();
                firmaBox.Text = (rdr[13]).ToString();
                AdresBox.Text = (rdr[14]).ToString();
                NIPBox.Text = (rdr[15]).ToString();
            }
        }
    }
}
