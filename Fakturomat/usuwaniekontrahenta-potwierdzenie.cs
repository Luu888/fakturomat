﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fakturomat
{
    
    public partial class usuwaniekontrahenta_potwierdzenie : Form
    {
        string index_pobrany;
        public usuwaniekontrahenta_potwierdzenie(string v)
        {
            InitializeComponent();
            index_pobrany = v;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            usuwanie_kontrahenta();
        }
        public void usuwanie_kontrahenta()
        {
            string connStr = "server=localhost;user=root;database=faktury;port=3306;password=";
            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();
            string sql = "DELETE from kontrahenci WHERE id_kontrahenta = " + index_pobrany;
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();
            this.Close();
            Form4.ActiveForm.Close();
        }
    }
}
