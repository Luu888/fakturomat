﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fakturomat
{
    public partial class Form7 : Form
    {
        public Form7()
        {
            InitializeComponent();
            label1.Text = "Pomyślnie dodano nową firmę";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form4 master = (Form4)Application.OpenForms["Form4"];
            master.pobierz_dane_kontrahenta_konkretnego();
            Form4.ActiveForm.Refresh();
            this.Close();
            Form6.ActiveForm.Close(); 
        }

    }
}
