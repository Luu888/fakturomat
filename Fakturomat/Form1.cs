﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace Fakturomat
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string[] kontrahent = new string[6];
        string[] firma = new string[4];
        List<string> kont = new List<string>();
        private void button1_Click(object sender, EventArgs e)
        {
            new Dane_do_faktury(kont[listBox1.SelectedIndex]).Show();
        }
        public void pobierz_dane_klientow()
        {
            //richTextBox1.Text = "";
            string index = kont[listBox1.SelectedIndex];
            string connStr = "server=localhost;user=root;database=faktury;port=3306;password=";
            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();
            string sql = "SELECT * FROM kontrahenci JOIN firma on fk_id_firmy=id_firmy where id_kontrahenta=" + index;
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                textBox1.Text = rdr[1].ToString();
                textBox2.Text = rdr[2].ToString();
                textBox3.Text = rdr[3].ToString();
                textBox4.Text = rdr[4].ToString();
                textBox5.Text = rdr[7].ToString();
                textBox6.Text = rdr[8].ToString();
                textBox7.Text = rdr[9].ToString();
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            zaladuj_kontrahentow_z_bazy();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            new Form2().Show();
        }
        public void zaladuj_kontrahentow_z_bazy()
        {
            listBox1.Items.Clear();
            kont.Clear();
            string connStr = "server=localhost;user=root;database=faktury;port=3306;password=";
            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();
            string sql = "SELECT * FROM kontrahenci ORDER BY nazwisko_kontrahenta";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                listBox1.Items.Add((rdr[1] + " " + rdr[2]).ToString());
                kont.Add(rdr[0].ToString());
            }
        }
        private void button3_Click(object sender, EventArgs e)
        {
            zaladuj_kontrahentow_z_bazy();
        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            pobierz_dane_klientow();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            button1.Enabled = true;
            button4.Enabled = true;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            new Form4(kont[listBox1.SelectedIndex]).Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            pokaz_faktury();
        }
        public void pokaz_faktury()
        {
            new wykaz_faktur().Show();
        }
    }
}
