﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fakturomat
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
            label1.Text = "Pomyślnie dodano nowego kontrahenta";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            Form2.ActiveForm.Close();
        }
       
    }
}
