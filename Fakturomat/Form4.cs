﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fakturomat
{
    public partial class Form4 : Form
    {
        string index_pobrany;
        List<string> firmy = new List<string>();
        public Form4(string v)
        {
            InitializeComponent();
            index_pobrany = v;
            pobierz_dane_kontrahenta_konkretnego();
        }
        public void pobierz_dane_kontrahenta_konkretnego()
        {
            string connStr = "server=localhost;user=root;database=faktury;port=3306;password=";
            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();
            string sql = "SELECT * FROM kontrahenci JOIN firma on fk_id_firmy=id_firmy WHERE id_kontrahenta = " + index_pobrany;
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                ImieBox.Text = rdr[2].ToString();
                nazwiskoBox.Text = rdr[1].ToString();
                telefonBox.Text = rdr[3].ToString();
                emailBox.Text = rdr[4].ToString();
                comboBox1.Text = rdr[7].ToString();
            }
            conn.Close();
            conn.Open();
            string sql2 = "SELECT * FROM firma";
            MySqlCommand cmd2 = new MySqlCommand(sql2, conn);
            MySqlDataReader rdr2 = cmd2.ExecuteReader();
            while (rdr2.Read())
            {
                comboBox1.Items.Add(rdr2[1]);
                firmy.Add(rdr2[0].ToString());
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                ImieBox.Enabled = true;
                nazwiskoBox.Enabled = true;
                telefonBox.Enabled = true;
                emailBox.Enabled = true;
                comboBox1.Enabled = true;
                button1.Enabled = true;
                button2.Enabled = true;
                button3.Enabled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            new Form5().Show();
            aktualizuj_dane();
            pobierz_dane_kontrahenta_konkretnego();
        }
        public void aktualizuj_dane()
        {
            string connStr = "server=localhost;user=root;database=faktury;port=3306;password=";
            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();
            string sql = "UPDATE kontrahenci SET nazwisko_kontrahenta='"+nazwiskoBox.Text+"', imie_kontrahenta='"+ImieBox.Text+"', telefon='"+telefonBox.Text+"', email='"+emailBox.Text+"', fk_id_firmy='"+ firmy[comboBox1.SelectedIndex] + "' WHERE id_kontrahenta = " + index_pobrany;
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();
            
        }

        private void Form4_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form1 master = (Form1)Application.OpenForms["Form1"];
            master.button3.PerformClick();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            new Form6().Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            new usuwaniekontrahenta_potwierdzenie(index_pobrany).Show();
        }

        private void Form4_Load(object sender, EventArgs e)
        {

        }
    }
}
