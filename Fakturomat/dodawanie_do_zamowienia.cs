﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fakturomat
{
    public partial class dodawanie_do_zamowienia : Form
    {
        List<string> id_produktow = new List<string>();
        List<string> cena_produktow = new List<string>();
        public dodawanie_do_zamowienia()
        {
            InitializeComponent();
        }

        private void dodawanie_do_zamowienia_Load(object sender, EventArgs e)
        {
            string connStr = "server=localhost;user=root;database=faktury;port=3306;password=";
            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();
            string sql = "SELECT * FROM produkty";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                comboBox1.Items.Add(rdr[1]);
                id_produktow.Add(rdr[0].ToString());
                cena_produktow.Add(rdr[2].ToString());
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Dane_do_faktury master = (Dane_do_faktury)Application.OpenForms["Dane_do_faktury"];
            master.dataGridView1.Rows.Add(id_produktow[comboBox1.SelectedIndex], comboBox1.SelectedItem.ToString(), cena_produktow[comboBox1.SelectedIndex], textBox1.Text.ToString());
            double kwota = Convert.ToDouble(master.label10.Text);
            kwota += Convert.ToDouble(cena_produktow[comboBox1.SelectedIndex])*Convert.ToInt32(textBox1.Text);
            master.label10.Text = kwota.ToString();
            this.Close();
        }
    }
}
